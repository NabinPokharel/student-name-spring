package com.demo.studentInfo;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author nabin
 * @Date 6/21/18
 **/
public class Student {
    private List<String> student;

    public void setStudent(List<String> student) {
        this.student = student;
    }

    public void showStudent(){

        System.out.println("\nPrinting in sorted order:");
        student.stream().sorted().forEach(System.out::println);

    }
}
