package com.demo.studentInfo;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @Author nabin
 * @Date 6/21/18
 **/
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of Students:");
        int num = sc.nextInt();

        List<String> list = new ArrayList<>();
        for (int i = 1; i <= num; i++) {
            System.out.println("Enter name of " + i + "st Student");
            list.add(sc.next());
        }
        /*list.stream().sorted().collect(Collectors.toList()).forEach(System.out::println);*/
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("resources/ApplicationContext.xml");
        Student student = (Student) applicationContext.getBean("student");
        student.setStudent(list);
        student.showStudent();

       //                                            new FileSystemResource("src/resources/ApplicationContext.xml")
       /* BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("resources/ApplicationContext.xml"));
        Student student1 = (Student) beanFactory.getBean("student");
        student1.setStudent(list);
        student1.showStudent();*/

    }
}
